# ODS Full Stack Coding Assignment

## Assignment

Create a web application that allows a user to search for flights and display the results in a tabular view.

## Features

1. Allow the user to enter a station (destination or origin) to search flights. Display the results in a table.

2. Provide an auto-suggest feature for station.

3. Provide two RESTful endpoints supporting the functionality listed in steps 1 and 2.

# Amrut Kulkarni Readme
**Overview**
This project uses a Django/SQlite backend + an Angular frontend. It uses Django Restful API and Angular HttpClient services to transfer data between the backend and frontend. It allows users to search for flights based on flight origin or destination using Django's Model.objects.all() to get all entries in an SQLite table, request.GET.get() to set what column to filter by, and model.filter() to pull query the db table based on the filtered data. Auto-suggest in the search bars was achieved by querying the database table for all the values in the origin_full_name and destination_full_name columns on the django backend, sending that data to the angular frontend, filling a <datalist> with that data using *ngFor, and linking that <datalist> to the search bar <input>

**Command Line Download list**
* pip3 install django
* pip3 install pandas # (to load db)
* pip3 install djangorestframework
* npm install -g @angular/cli

**Project Run Instructions**
* cd into SQLiteBackend
* run python3 manage.py runserver
* open up http://127.0.0.1:8000/ in a browser to see the JSON info the Django backend returns
* Open a new terminal window
* cd into FlightList
* run ng serve
* open up http://localhost:4200/ to see the frontend

**Notes**
* The database takes a some time to query from the SQLite database, so if there isn't anything on the page on load, wait a few seconds for the query to complete
* To view the contents of db.sqlite3, use https://sqlitebrowser.org/ to open it
* I'm submitting a .zip file of the project because both github and gitlab would not upload the actual code for my Angular frontend, so it would be unaccessible if you cloned or downloaded this repo